import json

from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from rest_framework import generics

from devices.models import Sensor, SensorType
from devices.serializers import SensorSerializer, SensorTypeSerializer
from homes.models import Home
from homes.serializers import HomeSerializer


@csrf_exempt
def login(request):
    """
    Uses the credentials passed to check can be logged in.
    :param request: Post request containing credentials and of the data
    :return: Json containing  a key 'status'  which indicates status of user or request
    """
    passed_json = json.loads(request.body)
    try:
        username = passed_json['username']
        password = passed_json['password']
    except KeyError:
        return JsonResponse({'status': 'Not a valid request',
                             'Help': 'Ensure username and password were keys in json passed in.'})
    user = authenticate(**{'username': username, 'password': password})
    if user is None:
        return JsonResponse({'status': 'Invalid credentials'})
    return JsonResponse({'status': 'Valid credentials', 'user_id': user.id, 'user_name': user.username})



class HomeList(generics.ListAPIView):
    """
    Returns list of jsons. Each json is a home saved in the database.
    Note list can be empty.
    """
    serializer_class = HomeSerializer

    def get_queryset(self):
        """
        Returns homes belonging to a user who is identified
        by the username argument of the url.
        """
        owner_username = self.kwargs['owner_username']
        return Home.objects.filter(owner__username=owner_username)


class HomeDetail(generics.RetrieveAPIView):
    """
    Returns details of a particular home identified by primary key part of the url.
    """
    serializer_class = HomeSerializer
    queryset = Home.objects.all()


class HomeUpdate(generics.UpdateAPIView):
    serializer_class = HomeSerializer
    queryset = Home.objects.all()

class SensorUpdate(generics.UpdateAPIView):
    serializer_class = SensorSerializer
    queryset = Sensor.objects.all()

class SensorListForHome(generics.ListAPIView):
    """
    Returns list of jsons. Each json is a sensor saved in the database.
    Note list can be empty.
    """
    serializer_class = SensorSerializer

    def get_queryset(self):
        """
        Returns sensors that are placed in a home specified by home_id.
        """
        home_id = self.kwargs['home_id']
        return Sensor.objects.filter(home__id=home_id)


class SensorListForUser(generics.ListAPIView):
    """
    Returns list of jsons. Each json is a sensor saved in the database.
    Note list can be empty.
    """
    serializer_class = SensorSerializer

    def get_queryset(self):
        """
        Gets sensors owned by user with username passed in via
         the url (username is the last parameter of url )
        """
        owner_username = self.kwargs['owner_username']
        return Sensor.objects.filter(owner__username=owner_username)


class SensorDetail(generics.RetrieveAPIView):
    """
    Returns details of a particular sensor identified by primary key part of the url.
    """
    serializer_class = SensorSerializer
    queryset = Sensor.objects.all()


class SensorOfTypeForUser(generics.ListAPIView):
    """
    Get sensors of a particular type.
    """
    serializer_class = SensorSerializer

    def get_queryset(self):
        type_id = self.kwargs['type_id']
        username = self.kwargs['username']
        return Sensor.objects.filter(sensor_type__id=type_id,owner__username=username)


class SensorTypeForUserList(generics.ListAPIView):
    """
    Get a list of the sensor types For a user.
    """
    serializer_class = SensorTypeSerializer
    def get_queryset(self):
        username = self.kwargs['username']
        user_sensors = Sensor.objects.filter(owner__username=username).distinct('sensor_type')
        user_sensor_types = [user_sensor.sensor_type for user_sensor in user_sensors]
        return user_sensor_types



class SensorTypeDetail(generics.RetrieveAPIView):
    """
    Get details about a particular sensor type.
    """
    serializer_class = SensorTypeSerializer
    queryset = SensorType.objects.all()
