from django.urls import path

from . import views

urlpatterns=[
    path('login', views.login, name='api_login'),
    path('homes/<int:pk>/', views.HomeDetail.as_view(), name='home_detail'),
    path('homes/<int:pk>/update', views.HomeUpdate.as_view(), name="home_update"),
    path('homes/<str:owner_username>/', views.HomeList.as_view(), name='home_list'),
    path('homes/<int:home_id>/sensors', views.SensorListForHome.as_view(), name='home_sensor_list'),
    path('sensors/<int:pk>/', views.SensorDetail.as_view(), name='sensor_detail'),
    path('sensors/<int:pk>/update', views.SensorUpdate.as_view()),
    path('sensors/<str:owner_username>/', views.SensorListForUser.as_view(), name='user_sensor_list'),
    path('sensors/types/<int:type_id>/<str:username>', views.SensorOfTypeForUser.as_view(), name='sensors_of_type_list'),
    path('sensortypes/<str:username>', views.SensorTypeForUserList.as_view(), name='sensor_type_for_user_list'),
    path('sensortypes/<int:pk>', views.SensorTypeDetail.as_view(), name='sensor_type_detail')
]
