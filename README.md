# Introduction
This is the branch for deployed version of the grandeur smart home automation software. This was depolyed on an AWS EC2 instance running ubuntu.

## Setup
- Create docker volumes run `docker volume --create=db_data && docker volume --create=es_data`.
- To start simply use run `docker compose up --build -d`
