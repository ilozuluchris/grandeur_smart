import json
import logging
import threading
import zlib

from cryptography.fernet import Fernet
from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import NoBrokersAvailable



def produce_kafka(kafka_brokers, topic, data, retries=0):
    def on_send_success(record_metadata):
        print("data pushed")
        print("Topic is {}".format(record_metadata.topic))
        print("Partition is {}".format(record_metadata.partition))
        print("Offset is {}".format(record_metadata.offset))


    def on_send_error(excp):
        logging.error('I am an errback', exc_info=excp)
        # handle exception
    if retries >= 3:
        # todo: push to ssdb
        print("COULD NOT PUSH TO KAFKA ")
        return
    try:
        producer = KafkaProducer(bootstrap_servers=kafka_brokers)
        # produce asynchronously with callbacks
        future = producer.send(topic, data).add_callback(on_send_success).add_errback(on_send_error)
        # block until all async messages are sent
        #producer.flush()
        future.get(timeout=60)
    except NoBrokersAvailable:
        retries+=1
        produce_kafka(kafka_brokers, topic, data, retries)


def compress_data(data):
    """
    Compress data using zlib. Returns data in bytes
    :param data: data to compress
    :return:
    """
    data_in_bytes = bytes(data, encoding='UTF-8')
    return zlib.compress(data_in_bytes)

def decompress_data(data):
    """
    Decompress data using zlib.
    :param data: Data  to decompress
    :type data: Byte
    :return: Decompressed data in byte form
    """
    return zlib.decompress(data)


def encrypt_data(data):
    f = Fernet(b'aez4M61n7U1qAR9TRmKDEGtCnI-PGxBg3ai_s9mGiqg=')
    return f.encrypt(data)



def decrypt_data(data):
    f = Fernet(b'aez4M61n7U1qAR9TRmKDEGtCnI-PGxBg3ai_s9mGiqg=')
    return f.decrypt(data)




class GsConsumer(threading.Thread):
    def __init__(self, kafka_brokers, topics):
        threading.Thread.__init__(self)
        self.topics = topics
        self.kafka_brokers = kafka_brokers

    def run(self):
        print("THREAD STARTING")
        self._start_kafka_consumer()

    def _start_kafka_consumer(self):
        while True:
            print("....WAITING FOR MSGS...")
            try:
                self.consumer = KafkaConsumer(bootstrap_servers=self.kafka_brokers,fetch_max_wait_ms=1000,)
                self.consumer.subscribe(self.topics)
                for kafka_msg in self.consumer:
                    if kafka_msg.topic == "homes":
                        self._process_home_msg(kafka_msg)
                    if kafka_msg.topic == "delete_homes":
                        self._process_home_delete(kafka_msg)
            except Exception as e:
                print(e)
            self._start_kafka_consumer()

    def _process_home_msg(self, kafka_msg):
        from homes.utils import save_received_home
        try:
            home_data = decompress_data(decrypt_data(kafka_msg.value))
            save_received_home(home_data)
        except Exception as e:
            print(e)

    def _process_home_delete(self, kafka_msg):
        from homes.utils import delete_received_home
        home_data = decompress_data(decrypt_data(kafka_msg.value))
        delete_received_home(home_data)
