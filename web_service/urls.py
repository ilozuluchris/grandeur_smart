from django.conf import settings

from django.conf.urls.static import static
from django.urls import include, path
from django.views.generic import TemplateView

from django.contrib import admin
from django.contrib.flatpages import views

urlpatterns = [
    path("", TemplateView.as_view(template_name="homepage.html"), name="home"),
    path("admin/", admin.site.urls),
    path("account/", include("account.urls")),
    path("welcome", TemplateView.as_view(template_name="welcome.html"), name="welcome"),
    path("homes/", include('homes.urls')),
    path("sensors/", include("devices.urls")),
    path("api/", include('api.urls')),
]
urlpatterns +=[
    path('about-us/', views.flatpage, {'url':'/about-us/'}, name='about_us'),
    path('contact-us/', views.flatpage, {'url':'/contact-us/'}, name='contact_us')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
