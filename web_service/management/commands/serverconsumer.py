from django.conf import settings as django_settings
from django.core.management.base import BaseCommand, CommandError

from web_service.utils import GsConsumer

class Command(BaseCommand):
    help = "Starts the kafka consumer on the server side."

    def handle(self, *args, **options):
        try:
            self.serverconsumer_topics = ['sensors', 'homes', 'delete_homes']
            c = GsConsumer(topics=self.serverconsumer_topics, kafka_brokers=django_settings.KAFKA_BROKERS)
            c.start()
            self.stdout.write(self.style.SUCCESS("Consuming msgs from {}").format("sensors and homes"))

        except Exception:
            self.stderr.write(Exception.message)
