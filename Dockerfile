#FROM python:3.6
#ENV PYTHONUNBUFFERED 1
#RUN mkdir /code
#WORKDIR /code
#COPY requirements.txt /code/
#RUN pip install -r requirements.txt
#COPY . /code/
#RUN chmod +x start.sh
#EXPOSE 8000
##CMD ["python","manage.py", "runserver", "0.0.0.0:8000"]
#CMD ["/bin/bash","start.sh"]

FROM alpine:3.6

ENV FLASK_APP run_api.py


WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 librdkafka-dev postgresql-libs
RUN apk add jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev
RUN apk add --virtual .build-deps \
    gcc \
    musl-dev \
    python3-dev \
    postgresql-dev \
    libffi-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

ENV PYTHONUNBUFFERED 1

COPY start.sh manage.py ./

COPY  web_service web_service
COPY api api
COPY devices devices
COPY fixtures fixtures
COPY flatpages flatpages
COPY homes homes
COPY static static


COPY . /


RUN chmod +x start.sh

EXPOSE 8000
ENTRYPOINT ["./start.sh"]

