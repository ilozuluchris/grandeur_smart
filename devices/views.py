from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Q
from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import  get_object_or_404, render, reverse, redirect

from account.decorators import login_required

from .forms import UpdateSensorForm
from .models import Sensor
from .signals import  fetch_sensor_actions_in_es




def searched_sensors(user_sensors, parameter_to_query_with):
    return user_sensors.filter(
                   Q(short_name__icontains=parameter_to_query_with) |
                   Q(name__icontains=parameter_to_query_with) |
                   Q(state__icontains=parameter_to_query_with) |
                   Q(home__name__icontains=parameter_to_query_with) |
                   Q(sensor_type__name__icontains=parameter_to_query_with)
    ).distinct()


@login_required()
def index(request):
    user_sensors = Sensor.objects.filter(owner_id=request.user.id)
    query = request.GET.get("q")
    if not user_sensors:
        return render(request,'sensor_list.html',{'sensors':[]})
    if user_sensors and query:
        queried_sensors = searched_sensors(user_sensors, query)
        return render(request, 'sensor_list.html', {'sensors':queried_sensors})
    return render(request,'sensor_list.html',{'sensors':user_sensors})


@login_required()
def update_sensor(request, pk):
    sensor = get_object_or_404(Sensor, pk=pk)
    if sensor.owner.username == request.user.username:
        if request.method == "POST":
            form = UpdateSensorForm(request.POST, instance=sensor)
            if form.is_valid():
                form.save()
                return redirect(reverse('sensor_list'))
        else:
            form = UpdateSensorForm(instance=sensor)
        return render(request, 'update_sensor.html', {'form': form})
    else:
        return HttpResponseForbidden('You do not own this sensor, so you can not update it')

@staff_member_required()
def show_sensor_data_in_es(request):
    if request.method == "GET":
        res = fetch_sensor_actions_in_es()
        return JsonResponse(res, safe=False)
