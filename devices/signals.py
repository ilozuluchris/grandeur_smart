from datetime import datetime
import json

from django.conf import settings as django_settings
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver

from elasticsearch import Elasticsearch

from web_service.utils import compress_data, encrypt_data, produce_kafka
from .models import Sensor, SensorType
from .serializers import SensorSerializer, SensorTypeSerializer


@receiver(post_save, sender=Sensor)
def push_saved_sensor(sender, instance, *args, **kwargs):
    print("...........SAVED SENSOR TIME TO PUSH...........")
    instance_data = SensorSerializer(instance).data
    data_to_push = sensor_data_to_push(instance_data, "save")
    compressed_data = compress_data(json.dumps(data_to_push))
    encrypted_data = encrypt_data(compressed_data)
    index_sensor_action_in_es(data_to_push)
    produce_kafka(kafka_brokers=django_settings.KAFKA_BROKERS,
    topic=data_to_push['username'], data=encrypted_data)


def sensor_data_to_push(instance_data, action):
    """
    Prepare data for publishing to kafka
    :param instance_data_in_json:
    :return: Dict that helps in pushing
    """
    data = {}
    current_sensor = Sensor.objects.get(id=instance_data['id'])
    data['username'] = current_sensor.owner.username
    data['home_name'] = current_sensor.home.name
    data['sensor_type_data'] = SensorTypeSerializer(current_sensor.sensor_type).data
    data['model'] = "Sensor" # so to know local side knows what  model to save.
    data['model_action'] = action # covers creation and updating
    data['actual_sensor'] = {}
    passed_fields = ['owner','id', 'home','sensor_type']
    for field_name in instance_data:
        if field_name not in passed_fields:
            data['actual_sensor'][field_name] = instance_data[field_name]
    return data



@receiver(pre_delete, sender=Sensor)
def push_deleted_sensor(sender, instance, *args, **kwargs):
    print("...........Deleted SENSOR TIME TO PUSH...........")
    instance_data = SensorSerializer(instance).data
    data_to_push = sensor_data_to_push(instance_data, "delete")
    compressed_data = compress_data(json.dumps(data_to_push))
    encrypted_data = encrypt_data(compressed_data)
    index_sensor_action_in_es(data_to_push)
    produce_kafka(kafka_brokers=django_settings.KAFKA_BROKERS,
    topic=data_to_push['username'], data=encrypted_data)


def index_sensor_action_in_es(data):
    data['timestamp'] = datetime.now()
    es = Elasticsearch(hosts=django_settings.ES_NODES)
    es.index(index="sensors", doc_type="actions", body=data)


def fetch_sensor_actions_in_es():
    es = Elasticsearch(hosts=django_settings.ES_NODES)
    res = es.search(index="sensors", doc_type="actions",
                    body={"sort": [{"timestamp": {"order": "desc"}}],
                          'query': {'match_all': {}}})
    return [result['_source'] for result in res['hits']['hits']]
