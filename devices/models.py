from django.db import models
from django.conf import settings as django_settings
from django.db.models.signals import post_save
from django.contrib.postgres.fields import JSONField


from homes.models import Home


class SensorType(models.Model):
    name = models.CharField(max_length=30, unique=True)
    extra_info = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Sensor(models.Model):
    SENSOR_CHOICES= (('ON','ON'),
                     ('OFF', 'OFF')
                     )
    sensor_type = models.ForeignKey(SensorType, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    short_name = models.CharField(max_length=10)
    owner = models.ForeignKey(django_settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mqtt_topic = models.CharField(max_length=50, default="")
    state = models.CharField(choices=SENSOR_CHOICES, max_length=3)
    data = JSONField(help_text='Value to be added must in the form of {"key1":"value1", "key2":"value2"}',
                     blank=True, null=True)
    home = models.ForeignKey(Home, on_delete=models.CASCADE)

    def __str__(self):
        return self.owner.username + "'s " + self.short_name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, *args, **kwargs):
        if self.owner.id != self.home.owner.id:
            raise Exception('Owner of sensor and owner of home are not the same.')
        super().save(*args, **kwargs)

    class Meta:
        unique_together = ("owner","home","name")

