# Generated by Django 2.0.3 on 2018-11-14 16:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0005_auto_20181114_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensor',
            name='state',
            field=models.CharField(choices=[('ON', 'ON'), ('OFF', 'OFF')], max_length=3),
        ),
    ]
