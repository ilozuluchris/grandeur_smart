# Generated by Django 2.0.3 on 2018-11-14 12:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0002_auto_20181105_2316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensor',
            name='mqtt_topic',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='name',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='sensor',
            name='short_name',
            field=models.CharField(max_length=10),
        ),
    ]
