from django.urls import path

from .views import index, show_sensor_data_in_es, update_sensor


urlpatterns = [
    # add other patterns gere
    path('<int:pk>/update', update_sensor, name='update_sensor'),
    path('all-actions', show_sensor_data_in_es, name='all_sensor_actions'),
    path('', index, name='sensor_list')
]
