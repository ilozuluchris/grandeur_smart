from django.contrib import admin
from django.db import models
# Register your models here.

from .models import Sensor, SensorType

#TODO try to use js to change values of the  home selected using the user selected
#TODO SECURITY IN KAFKA and GS COLORS

from django.contrib import admin


admin.site.register(SensorType)

@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    search_fields = ["owner__username", "name", "home__name", "sensor_type__name"]
