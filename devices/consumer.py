#from kafka import KafkaConsumer
import json
import time


import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def on_publish(client, userdata, mid):
    print("Message has been published")
    print('mid value is %d' % mid)
    print(userdata)
    print(client)

def pub_to_mqtt(topic, received_data,  broker="broker.hivemq.com", port=1883):
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.on_publish = on_publish
    client.connect(broker, port, 60)
    client.loop_start()
    client.publish(topic, received_data, qos=1, retain=True)

    # Blocking call that processes network traffic, dispatches callbacks and
    # handles reconnecting.
    # Other loop*() functions are available that give a threaded interface and a
    # manual interface.



# def consume_from_kafka():
#     consumer = KafkaConsumer('test',
#                              group_id='my-group', bootstrap_servers=['localhost:9092'])
#     while True:
#         for message in consumer:
#             # message value and key are raw bytes -- decode if necessary!
#             # e.g., for unicode: `message.value.decode('utf-8')`
#             print ("%s:%d:%d: value=%s" % (message.topic, message.partition,
#                                                   message.offset,
#                                                   message.value))
#             received_data = message.value
#             mqtt_topic = json.loads(received_data.decode('ascii'))['mqtt_topic']
#             print("\n")
#             print(mqtt_topic)
#             print(type(received_data))
#             pub_to_mqtt(mqtt_topic,received_data)

# import threading
# class Consumer():
#     def __init__(self, broker, topic):
#         self.topic = topic
#         self.broker = broker
#         #group_id for consumers belonging to the same users
#         self.consumer = KafkaConsumer(self.topic, group_id='my-group', bootstrap_servers=self.broker)
#         self.work()
#     def work(self):
#         while True:
#             print(".........consumer ready.............")
#             time.sleep(0.5)
#             print(".........waiting for messages..........")
#             for message in self.consumer:
#                 # message value and key are raw bytes -- decode if necessary!
#                 # e.g., for unicode: `message.value.decode('utf-8')`
#                 print("%s:%d:%d: value=%s" % (message.topic, message.partition,
#                                               message.offset,
#                                               message.value))
#                 received_data = message.value
#                 mqtt_topic = json.loads(received_data.decode('ascii'))['mqtt_topic']
#                 print(mqtt_topic)
#                 print("\n")
#                 pub_to_mqtt(mqtt_topic, received_data)
#
#
# # def full_pub():
# #     # c,mqtt_topic = consume_from_kafka()
# #     pass
# def main():
#     Consumer(broker='localhost:9092', topic='test')
#
# if __name__ == "__main__":
#     main()