from django.db.models.signals import pre_save, post_save,pre_delete, post_delete
from django.test import TestCase
from django.contrib.auth.models import User

from account.models import Account
import factory
# Create your tests here.
from .models import Sensor, SensorType
from homes.models import Home


class SensorModelTests(TestCase):
    @factory.django.mute_signals(pre_save, post_save, post_delete, pre_delete)
    def setUp(self):
        self.sensor_type = SensorType.objects.create(**dict(name="motion_sensor"))

        self.user_1 = User.objects.create_user(**dict(username='test_user', password='test_password', email='user1@test.com'))
        self.user_2 = User.objects.create_user(**dict(username='test_user2', password='test_password', email='user2@test.com'))
        # self.account = Account.create(user=self.user)
        self.home = Home.objects.create(**dict(owner=self.user_1, name="home", address="somewhere", city="lon", country="mars"))

    @factory.django.mute_signals(pre_save, post_save, post_delete, pre_delete)
    def test_sensor_created(self):
        """
        Can new sensors be created
        """
        data = {'sensor_type':self.sensor_type,
                'name': 'test_sensor',
                'short_name': 'TS',
                'owner': self.user_1,
                'mqtt_topic': 'test-topic',
                'state': Sensor.SENSOR_CHOICES[0][0],
                'home': self.home,
                'data': {"key1":"value"}}
        Sensor.objects.create(**data)
        self.assertEqual(len(Sensor.objects.all()), 1, "Sensor can not  be created")

    @factory.django.mute_signals(post_save, post_delete, pre_delete)
    def test_create_sensor_with_diff_user_and_home_owner(self):
        data = {'sensor_type': self.sensor_type,
                'name': 'test_sensor',
                'short_name': 'TS',
                'owner': self.user_2,
                'mqtt_topic': 'test-topic',
                'state': Sensor.SENSOR_CHOICES[0][0],
                'home': self.home,
                'data': {"key1": "value"}}
        with self.assertRaises(Exception) as error:
            Sensor.objects.create(**data)
        the_exception = error.exception
        self.assertEqual(str(the_exception), "Owner of sensor and owner of home are not the same.")

    @factory.django.mute_signals(pre_save, post_save, post_delete, pre_delete)
    def tearDown(self):
        User.objects.all().delete()
