from django.forms import ModelForm, Select, TextInput

from .models import Sensor

class UpdateSensorForm(ModelForm):
    class Meta:
        model = Sensor
        fields = ['name', 'state']
        widgets = {
            'name': TextInput(attrs={'readonly':'readonly', 'class':'form-control'}),
            'state': Select(attrs={'class': 'form-control'})
        }
