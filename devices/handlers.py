from django.conf import settings as django_settings
from django.db.models.signals import post_save
from rest_framework.renderers import JSONRenderer

from .serializers import SensorSerializer


# todo how to seperate signals
#
def push_sensor_config(sender, instance, *args, **kwargs):
    """

    :param sender:
    :param args:
    :param kwargs:
    :return:
    """
    sensor = SensorSerializer(instance)
    json_data = JSONRenderer().render(sensor.data) # is of type str
    #todo broker_details, topic should be read from the settings
    produce_kafka(json_data, 'localhost', 'test')

#
# def push_config_flame(sender, instance, *args, **kwargs):
#     """
#
#     :param sender:
#     :param args:
#     :param kwargs:
#     :return:
#     """
#     c = FlameSensorSerializer(instance)
#     json_data = JSONRenderer().render(c.data)  # is of type str
#     # todo broker_details, topic should be read from the settings
#     produce_kafka(json_data, 'localhost', 'test')
#
#
# def push_config_motion(sender, instance, *args, **kwargs):
#     """
#
#     :param sender:
#     :param args:
#     :param kwargs:
#     :return:
#     """
#     c = MotionSensorSerializer(instance)
#     json_data = JSONRenderer().render(c.data)  # is of type str
#     # todo broker_details, topic should be read from the settings
#     produce_kafka(json_data, 'localhost', 'test')


from kafka import KafkaProducer
from kafka.errors import KafkaError


def produce_kafka(data, broker, topic):
    producer = KafkaProducer(bootstrap_servers=broker)

    def on_send_success(record_metadata):
        print(record_metadata.topic)
        print(record_metadata.partition)
        print(record_metadata.offset)
    import logging
    def on_send_error(excp):
        logging.error('I am an errback', exc_info=excp)
        # handle exception

    # produce asynchronously with callbacks
    producer.send(topic,data).add_callback(on_send_success).add_errback(on_send_error)

    # block until all async messages are sent
    producer.flush()


