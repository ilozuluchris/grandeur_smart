from datetime import  datetime
import json
import random
import string

from elasticsearch import Elasticsearch

from django.db.models.signals import post_save, pre_delete
from django.conf import settings as django_settings
from account.models import Account

from .models import Home
from .serializers import HomeSerializer
from web_service.utils import compress_data, encrypt_data, decrypt_data, produce_kafka


def generate_security_key(instance, length=16):
    key = ''.join(random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length))
    klass = instance.__class__
    key_exists = klass.objects.filter(key=key).exists()
    if key_exists:
        return generate_security_key(instance)
    return key


def push_saved_home(sender, instance, *args, **kwargs):
    print("...........SAVED HOME TIME TO PUSH...........")
    instance_data  = HomeSerializer(instance).data
    data_to_push = home_data_to_push(instance_data,"save")
    compressed_data = compress_data(json.dumps(data_to_push))
    encrypted_data = encrypt_data(compressed_data)
    index_home_action_in_es(data_to_push)
    produce_kafka(kafka_brokers=django_settings.KAFKA_BROKERS,
                  topic=data_to_push['username'], data=encrypted_data)


def home_data_to_push(instance_data, action):
    """
    Prepare data for publishing to kafka
    :param instance_data_in_json:
    :return: Dict that helps in pushing
    """
    data = {}
    username = Home.objects.get(id=instance_data['id']).owner.username
    data['username'] = username
    data['model'] = "Home" # so to know local side knows what  model to save.
    data['model_action'] = action # covers creation and updating
    # copy over only the relevant data in a model
    for field_name in instance_data:
        if field_name != 'owner' and field_name != 'id':
            data[field_name] = instance_data[field_name]
    return data

def save_received_home(data):
    """
    Used to save homes received from local version
    :param data: Json string containing data for model instance
    :return:
    """
    unique_data, other_data = serialize_received_home(data)
    try:
        print("...........DISCONNECTING SIGNL SHOULD NOT SAVE")
        post_save.disconnect(push_saved_home, sender=Home) # disconnect to prevent continous lopping, todo how would this work when many people use it
        print("........Should not push.......................")
        c = Home.objects.update_or_create(**unique_data, defaults=other_data)
        print(c)
    except Exception:
        print(Exception.message)


def serialize_received_home(received_data):
    """
    Get actual data to be used to save received home
    :param received_data:
    :return:
    """
    data = json.loads(received_data)
    owner_id = Account.objects.get(user__username=data['username']).user_id
    unique_data = {'name':data['name'], 'key':data['key']}
    del data['username']
    del data['key']
    del data['name']
    data['owner_id'] = owner_id
    return  unique_data, data


def push_deleted_home(sender, instance, *args, **kwargs):
    print("...........DELETED MODEL TIME TO PUSH...........")
    instance_data  = HomeSerializer(instance).data
    data_to_push = home_data_to_push(instance_data,"delete")
    compressed_data = compress_data(json.dumps(data_to_push))
    encrypted_data = encrypt_data(compressed_data)
    index_home_action_in_es(data_to_push)
    produce_kafka(kafka_brokers=django_settings.KAFKA_BROKERS,
                  topic=data_to_push['username'], data=encrypted_data)



def delete_received_home(received_data):
    """
    Delete received
    :param received_data:
    :return:
    """
    unique_data, other_data = serialize_received_home(received_data)
    try:
        print("...........DISCONNECTING SIGNal...........")
        pre_delete.disconnect(push_deleted_home, sender=Home) # disconnect to prevent continous lopping, todo how would this work when many people use it
        print("........Should not push.......................")
        home = Home.objects.get(**unique_data)
        del_status =  home.delete()
        print(del_status)
    except Exception:
        print(Exception.message)


def index_home_action_in_es(data):
    try:
        data['timestamp'] = datetime.now()
        es = Elasticsearch(hosts=django_settings.ES_NODES)
        es.index(index="homes", doc_type="actions", body=data)
    except Exception as e:
        print(str(e))
        pass


def fetch_home_actions_in_es():
    es = Elasticsearch(hosts=django_settings.ES_NODES)
    res = es.search(index="homes", doc_type="actions",
                    body={"sort": [{"timestamp": {"order": "desc"}}],
                          'query': {'match_all': {}}})
    return [result['_source'] for result in res['hits']['hits']]
