from django.urls import path

from .views import create_home, index, HomeUpdateView, HomeDeleteView, show_home_data_in_es


urlpatterns = [
    path('create', create_home, name='create_home'),
    path('<int:pk>/update/', HomeUpdateView.as_view(), name='update_home'),
    path('<int:pk>/delete/', HomeDeleteView.as_view(), name='delete_home'),
    path('all-actions', show_home_data_in_es, name='all_home_actions'),
    path('', index, name='homes_list')
]
