from django.forms import ModelForm, TextInput

from .models import Home


class CreateHomeForm(ModelForm):
    class Meta:
        model = Home
        exclude = ['owner','key']
        widgets = {
            'country': TextInput(attrs={'value':'Nigeria', 'class':'form-control'}),
            'city': TextInput(attrs={'class':'form-control'}),
            'address': TextInput(attrs={'class':'form-control'}),
            'name': TextInput(attrs={'class':'form-control'})
        }


class UpdateHomeForm(ModelForm):
    class Meta:
        model = Home
        exclude = ['owner','key',]
        widgets = {
            'name': TextInput(attrs={'readonly':'readonly', 'class':'form-control'}),
            'country': TextInput(attrs={'class': 'form-control'}),
            'city': TextInput(attrs={'class': 'form-control'}),
            'address': TextInput(attrs={'class': 'form-control'}),
        }
