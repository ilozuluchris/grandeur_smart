from django.contrib import admin

from .models import Home

admin.site.register(model_or_iterable=Home)