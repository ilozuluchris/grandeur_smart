from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import  render, redirect
from django.urls import reverse_lazy
from django.views.generic.edit import  DeleteView, UpdateView
from django.contrib.admin.views.decorators import staff_member_required
from account.decorators import login_required

from .forms import CreateHomeForm, UpdateHomeForm
from .models import Home
from .utils import fetch_home_actions_in_es


def set_home_permissions_for_user(user):
    home_content_type = ContentType.objects.get_for_model(Home)
    delete_permission = Permission.objects.get(
        codename='delete_home', content_type=home_content_type)
    change_permission = Permission.objects.get(
        codename='change_home', content_type=home_content_type)

    user.user_permissions.set([delete_permission, change_permission])


@login_required()
def create_home(request):
    if request.method == "POST":
        form = CreateHomeForm(request.POST)
        if form.is_valid():
            new_home = form.save(commit=False)
            new_home.owner_id = request.user.id
            new_home.save()
            owner = User.objects.get(id=request.user.id)
            set_home_permissions_for_user(owner)
            return redirect('/homes')
    else:
        form = CreateHomeForm()
    return render(request, 'create_home.html',{'form':form})


def searched_homes(user_homes, parameter_to_query_with):
    return user_homes.filter(
                   Q(address__icontains=parameter_to_query_with) |
                   Q(city__icontains=parameter_to_query_with) |
                   Q(name__icontains=parameter_to_query_with) |
                   Q(country__icontains=parameter_to_query_with)
    ).distinct()


@login_required()
def index(request):
    user_homes = Home.objects.filter(owner_id=request.user.id)
    query = request.GET.get("q")
    if not user_homes:
        return render(request,'homes_list.html',{'homes':[]})
    if user_homes and query:
        queried_homes = searched_homes(user_homes, query)
        return render(request, 'homes_list.html', {'homes': queried_homes})
    return render(request,'homes_list.html',{'homes':user_homes})


class HomeUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Home
    form_class =  UpdateHomeForm
    success_url = reverse_lazy('homes_list')
    template_name = 'update_home.html'
    permission_required = 'homes.change_home'
    raise_exception = True


class HomeDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Home
    success_url = reverse_lazy('homes_list')
    template_name = 'delete_home.html'
    permission_required = 'homes.delete_home'
    raise_exception = True

@staff_member_required()
def show_home_data_in_es(request):
    if request.method == "GET":
        res = fetch_home_actions_in_es()
        return JsonResponse(res, safe=False)
