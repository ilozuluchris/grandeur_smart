from django.db import models
from django.conf import settings



#todo reducing details from home list api
class Home(models.Model):
    owner = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=250)
    city = models.CharField(max_length=50)
    country = models.CharField(max_length=100)
    key = models.CharField(blank=True,unique=True, max_length=250)

    def __str__(self):
        return self.owner.username+"'s "+self.name

    class Meta:
        unique_together = ("owner", "name")


