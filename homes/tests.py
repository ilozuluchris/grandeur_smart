import  random
import string

import unittest
from django.test import Client
from homes.utils import generate_security_key
from django.urls import reverse
import time

class SimpleTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.user1_client = Client()
        self.admin2_client = Client()
        self.user1_client.login(username='user1', password='user1')
        self.admin2_client.login(username='admin2', password='user3')
        self.create_home_url = reverse("create_home")
        print(self.create_home_url)

    def test_home_pushing(self):
        length = 50
        for _ in range(1,50):
            name1 = random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            country1 = random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            city1 =random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            address1 = random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            data1 = {'name':name1,"country":country1, "city":city1, "address":address1}


            name2 = random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            country2 = random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            city2 =random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            address2 = random.SystemRandom().choices(string.ascii_lowercase + string.digits + string.ascii_uppercase, k=length)
            data2 = {'name': name2, "country": country2, "city": city2, "address": address2}



            user1_res = self.user1_client.post(self.create_home_url,follow=True ,data=data1)
            self.assertEqual(user1_res.status_code, 200)

            time.sleep(1)
            user2_res = self.admin2_client.post(self.create_home_url, follow=True, data=data2)
            self.assertEqual(user2_res.status_code, 200)
