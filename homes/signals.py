from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save,pre_delete

from .models import Home
from .utils import generate_security_key, push_saved_home, push_deleted_home

@receiver(pre_save, sender=Home)
def attach_key(sender, instance, *args, **kwargs):
    if not instance.key:
        instance.key = generate_security_key(instance)



post_save.connect(push_saved_home, sender=Home)
pre_delete.connect(push_deleted_home, sender=Home)