# Generated by Django 2.0.3 on 2018-07-20 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homes', '0002_auto_20180720_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='home',
            name='key',
            field=models.CharField(default='UnAVYpztAACHTXRP', max_length=250, unique=True),
        ),
    ]
