#!/bin/sh

sleep 5
source venv/bin/activate
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --no-input
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'pass')" | python manage.py shell
python manage.py serverconsumer &
gunicorn web_service.wsgi -w 3 --access-logfile - --error-logfile -  --bind 0.0.0.0:8000 -t 150
